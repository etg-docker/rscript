source ../settings.sh

echo "Testing loading of installed libraries."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}:/usr/src/myscripts" \
	$IMAGENAME \
		libraries.R
