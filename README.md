# etg-docker: ekleinod/rscript

## Quick reference

- **Maintained by:** [ekleinod](https://gitlab.com/etg-docker/rscript/)
- **Docker hub:** [ekleinod/rscript](https://hub.docker.com/r/ekleinod/rscript)
- **Where to get help:** [gitlab](https://gitlab.com/etg-docker/rscript/), [issue tracker](https://gitlab.com/etg-docker/rscript/-/issues)


## Supported tags

- `1.0.0`, `1.0.0-4.1.3`, `latest`


## What is this image?

This is a docker image for the R (statistics language) command line tool *Rscript*.
The image has the name `ekleinod/rscript`.
It is based on the excellent [official r-base image](https://hub.docker.com/_/r-base) and extends it with the plugins/extensions needed for my projects.

The additions are:

- data.table – <https://cran.r-project.org/web/packages/data.table/>
- ggplot2 – <https://cran.r-project.org/web/packages/ggplot2/>
- ggthemes – <https://cran.r-project.org/web/packages/ggthemes/>
- lubridate – <https://cran.r-project.org/web/packages/lubridate/>
- RColorBrewer – <https://cran.r-project.org/web/packages/RColorBrewer/>

For the complete changelog, see [changelog.md](changelog.md)


I could not add the following library, because of installation problems:

- readODS – <https://cran.r-project.org/web/packages/readODS/>


## How to use the image

You can use the image as follows:

~~~ bash
docker run --rm --interactive --tty --name <containername> --user "$(id -u):$(id -g)" --volume "${PWD}":/usr/src/myscripts ekleinod/rscript <command>
~~~

Please note, that `/usr/src/myscripts` is the work directory of the image.

For a simple test showing the rscript version, run

~~~ bash
$ docker run --rm --name rscript ekleinod/rscript --version
~~~

You can see examples at work in the test folder.
Clone the repository or download the folder.
Call them as follows:

~~~ bash
$ cd test
$ ./test_help.sh
$ ./test_hello-world.sh
$ ./test_version.sh
~~~


## Releases

The latest release will always be available with:

`ekleinod/rscript:latest`

There are two naming schemes:

1. `ekleinod/rscript:<internal>-<rscript>`

	Example: `ekleinod/rscript:1.0.0-4.1.3`

2. internal version number `ekleinod/rscript:<major>.<minor>.<patch>`

	Example: `ekleinod/rscript:1.0.0`


## Build locally

In order to build the image locally, clone the repository and call

~~~ bash
$ cd image
$ ./build_image.sh
~~~

## Git-Repository

The branching model regards to the stable mainline model described in <https://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the main branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.

## Copyright

Copyright 2022-2022 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
